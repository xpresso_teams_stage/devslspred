"""
This is the implementation of data preparation for sklearn
"""
import os
import sys

__author__ = "Naveen Sinha"

import logging
import pandas as pd
import numpy as np

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

logger = XprLogger("xgboost_data_prep",level=logging.INFO)


class XgboostDataPrep(AbstractPipelineComponent):
    """ Core implementation of data preparation required for
      rosssman store sales
  """

    def __init__(self, train_file_path, test_file_path, store_file_path):
        super().__init__(name="XgboostDataPrep")
        self.train_data = pd.read_csv(train_file_path)
        self.store_data = pd.read_csv(store_file_path)
        self.test_data = pd.read_csv(test_file_path)

        # Merging Store file into a single dataset for train and test
        self.combined_train_data = None
        self.combined_test_data = None
        logger.info("Data load completed")

    def prepare_date(self, data):
        data['Date'] = pd.to_datetime(data['Date'], errors='coerce')
        data['Date'] = data['Date'].dt.strftime('%d-%m-%Y')
        data['Year'] = pd.DatetimeIndex(data['Date']).year
        data['Month'] = pd.DatetimeIndex(data['Date']).month
        data['Date'] = pd.to_datetime(data['Date'])
        data['DayOfWeek'] = data['Date'].dt.weekday_name

    # Python Function to remove outliers
    def remove_outliers(self, df, column, min_val, max_val):
        col_values = df[column].values
        df[column] = np.where(
            np.logical_or(col_values <= min_val, col_values >= max_val), np.NaN,
            col_values)
        return df

    def start(self, run_name):
        super().start(xpresso_run_name=run_name)
        logger.info("Preparing data")
        self.combined_train_data = self.preprocess(data=self.train_data)
        self.combined_test_data = self.preprocess(data=self.test_data)
        self.completed()

    def send_metrics(self, status, combined_data):
        report_status = {"status": {"status": status},
                "metric" : { "rows": combined_data.shape[0],
                         "columns": combined_data.shape[1]
                         }}
        self.report_status(status=report_status)

    def preprocess(self, data):
        combined_data = pd.merge(data,
                                 self.store_data,
                                 on="Store")
        combined_data.fillna(0, inplace=True)
        logger.info(combined_data.isnull().values.any())
        logger.info(combined_data.head(5))
        self.send_metrics(status="Merged the data", combined_data=combined_data)

        self.prepare_date(data=combined_data)
        logger.info(combined_data.head(5))
        self.send_metrics(status="Data cleaning done", combined_data=combined_data)

        median_cd = combined_data['CompetitionDistance'].median(
            skipna=True)
        combined_data[
            'CompetitionDistance'] = combined_data.CompetitionDistance.mask(
            combined_data.CompetitionDistance == 0, median_cd)
        combined_data = combined_data[combined_data["Open"] != 0]
        logger.info(combined_data.head(5))
        self.send_metrics(status="Masking the data", combined_data=combined_data)

        # Removing Outlier
        q1 = combined_data['CompetitionDistance'].quantile(0.25)
        q3 = combined_data['CompetitionDistance'].quantile(0.75)
        iqr = q3 - q1
        max_value = q3 + 1.5 * iqr
        combined_data = self.remove_outliers(
            df=combined_data, column='CompetitionDistance',
            min_val=0, max_val=max_value)
        logger.info(combined_data.head(5))
        self.send_metrics(status="Removing outliers", combined_data=combined_data)

        combined_data.drop(["Open", "Date"], axis=1, inplace=True)
        equiv = {'a': 1, 'b': 2, 'c': 3}
        combined_data["NewAssortment"] = combined_data["Assortment"].map(equiv)

        equiv1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
        combined_data["NewStoreType"] = combined_data["StoreType"].map(equiv1)
        logger.info(combined_data.head(5))
        self.send_metrics(status="Converting assortment to inte",
                          combined_data=combined_data)
        return combined_data

    def completed(self, push_exp=False):
        output_dir = "/data/raw_dataset"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        self.combined_train_data.to_csv(os.path.join(output_dir,
                                                     "combined_train.csv"),
                                        index=False)
        self.combined_test_data.to_csv(
            os.path.join(output_dir, "combined_test.csv"),
            index=False)
        logger.info("Data saved")
        super().completed(push_exp=False)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    data_prep = XgboostDataPrep(train_file_path="/data/raw_dataset/train.csv",
                                test_file_path="/data/raw_dataset/test.csv",
                                store_file_path="/data/raw_dataset/store.csv")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
